#!/bin/bash
IPADDRESS=$(curl http://checkip.amazonaws.com)
APPNAME='ynot'

cd ~/baloon
git pull
truncate -s 0 "$APPNAME-data.json"
echo "{'ip-address':'http://$IPADDRESS:8080'}" >> "$APPNAME-data.json"
git add "$APPNAME-data.json"
git commit -m "ip-address-update"
git push git@gitlab.com:arkitroop/baloon.git
